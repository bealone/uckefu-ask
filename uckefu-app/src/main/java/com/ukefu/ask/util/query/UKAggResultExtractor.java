package com.ukefu.ask.util.query;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalDateHistogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;

import com.ukefu.ask.web.model.Topic;
import com.ukefu.ask.web.model.TopicComment;


public class UKAggResultExtractor extends UKResultMapper{
	
	private String term ;
	
	public UKAggResultExtractor(String term){
		this.term = term ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
		Aggregations aggregations = response.getAggregations();
		List<T> results = new ArrayList<T>();
		long total = 0 ;
		Aggregation a = aggregations.get(term);
		if(a instanceof Terms){
			Terms agg = aggregations.get(term) ;
			if(agg!=null){
				total = agg.getSumOfOtherDocCounts() ;
				if(agg.getBuckets()!=null && agg.getBuckets().size()>0){
					for (Terms.Bucket entry : agg.getBuckets()) {
						if(clazz.equals(Topic.class)){
							Topic topic = new Topic();
							topic.setCreater((String)entry.getKey());
							topic.setRowcount((int) entry.getDocCount());
							results.add((T) topic) ;
						}else if(clazz.equals(TopicComment.class)){
							TopicComment topicComment = new TopicComment();
							topicComment.setCreater((String)entry.getKey());
							topicComment.setRowcount((int) entry.getDocCount());
							results.add((T) topicComment) ;
						}
					}
				}
			}
		}else if(aggregations.get(term) instanceof InternalDateHistogram){
			InternalDateHistogram agg = aggregations.get(term) ;
			total = response.getHits().getTotalHits() ;
			if(agg!=null){
				if(agg.getBuckets()!=null){
					for (Histogram.Bucket entry : agg.getBuckets()) {
						if(clazz.equals(Topic.class)){
							Topic topic = new Topic();
							topic.setKey(((String)entry.getKey()).substring(0 , 10));
							topic.setRowcount((int) entry.getDocCount());
							results.add((T) topic) ;
						}	
					}
				}
			}
		}
		return new AggregatedPageImpl<T>(results, pageable, total);
	}
}
