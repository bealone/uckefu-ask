/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50549
Source Host           : localhost:3306
Source Database       : ukefu

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2017-05-08 11:38:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `uk_fans`
-- ----------------------------
DROP TABLE IF EXISTS `uk_fans`;
CREATE TABLE `uk_fans` (
  `id` varchar(32) NOT NULL,
  `creater` varchar(32) DEFAULT NULL,
  `createtime` date DEFAULT NULL,
  `updatetime` date DEFAULT NULL,
  `user` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of uk_fans
-- ----------------------------

-- ----------------------------
-- Table structure for `uk_user`
-- ----------------------------
DROP TABLE IF EXISTS `uk_user`;
CREATE TABLE `uk_user` (
  `id` varchar(32) NOT NULL,
  `language` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `secureconf` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `midname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `jobtitle` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `usertype` varchar(255) DEFAULT NULL,
  `rulename` varchar(255) DEFAULT NULL,
  `searchprojectid` varchar(255) DEFAULT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `creater` varchar(32) DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `memo` varchar(255) DEFAULT NULL,
  `updatetime` timestamp NULL DEFAULT NULL,
  `organ` varchar(32) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `passupdatetime` timestamp NULL DEFAULT NULL,
  `sign` text,
  `del` tinyint(4) DEFAULT '0',
  `uname` varchar(100) DEFAULT NULL,
  `musteditpassword` tinyint(4) DEFAULT NULL,
  `agent` tinyint(4) DEFAULT NULL,
  `skill` varchar(32) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `fans` int(11) DEFAULT NULL,
  `follows` int(11) DEFAULT NULL,
  `integral` int(11) DEFAULT NULL,
  `lastlogintime` datetime DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `deactivetime` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of uk_user
-- ----------------------------

-- ----------------------------
-- Table structure for `uk_userevent`
-- ----------------------------
DROP TABLE IF EXISTS `uk_userevent`;
CREATE TABLE `uk_userevent` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `username` varchar(32) DEFAULT NULL,
  `creater` varchar(32) DEFAULT NULL,
  `orgi` varchar(50) DEFAULT NULL,
  `maintype` varchar(32) DEFAULT NULL,
  `subtype` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `admin` tinyint(32) DEFAULT NULL,
  `access` tinyint(32) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `hostname` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `isp` varchar(32) DEFAULT NULL,
  `province` varchar(32) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `sessionid` varchar(32) DEFAULT NULL,
  `param` text,
  `times` int(11) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `ostype` varchar(50) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `model` varchar(10) DEFAULT NULL,
  `appid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of uk_userevent
-- ----------------------------
